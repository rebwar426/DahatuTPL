<?php
/**
 * Created by PhpStorm.
 * User: EbdulRahman
 * Date: 4/28/14
 * Time: 5:20 PM
 */

require_once("Config/Config.class.php");
require_once('Tokens.class.php');
require_once('VariableStory.class.php');
require_once('MathExpression.class.php');
require_once('Libraries/ModifiersSymbol.lib.php');

class Interpreter {
    private $tTokenStore=NULL;
    public function Interpreter($strFileName=''){
        if ($this->haveTags($strFileName))
            $this->evalSourceCode($strFileName);
        else
            $this->evalFile($strFileName);
    }
    public function evalFile($strFileName){
        $strFileName = Configuration::getTPLFilesPath() . $strFileName;
        $this->tTokenStore = new Tokens($strFileName);
    }
    public function evalSourceCode($strTPLSourceCode){
        $this->tTokenStore = new Tokens($strTPLSourceCode,FALSE);
    }
    private function parseCode(){
        $strInterpretCode = '';
        $pTokens = &$this->tTokenStore;
        while(!$pTokens->itemOver()){
            $tToken = $pTokens->nextToken();
            switch($tToken->getAction()){
                case DTA_PRINT_OUT:
                    switch($tToken->getType()){
                        case DTT_HTML_TAG:
                            $strInterpretCode .= $tToken->getProperty('tag');
                            break;
                        case DTT_ERROR:
                            $strError = '<p style="color: red">'.$tToken->getProperty('error').'</p>';
                            $strInterpretCode .= $strError;
                            break;
                        case DTT_VARIABLE:
                            $anyValue = VariableStory::getVariable($tToken->getProperty('variable'));
                            $anyValue = $this->doModifier($anyValue, $tToken->getProperty('modifier'));
                            $strInterpretCode .= $anyValue;
                            break;
                        case DTT_EXPRESSION:
                            $anyValue = $tToken->getProperty('expression');
                            $anyValue = $this->parseExpression($anyValue);
                            $mathExpression = new MathExpression($anyValue);
                            $anyValue = $mathExpression->getResult();
                            $anyValue = $this->doModifier($anyValue,$tToken->getProperty('modifier'));
                            $strInterpretCode .= $anyValue;
                            break;
                    }
                    break;
                case DTA_ASSIGN:
                    $strType = $tToken->getProperty('type');
                    if ($strType=='fromvar')
                        $anyValue = VariableStory::getVariable($tToken->getProperty($strType));
                    elseif ($strType=='expression'){
                        $anyValue = $tToken->getProperty($strType);
                        $anyValue = $this->parseExpression($anyValue);
                        $mathExpression = new MathExpression($anyValue);
                        $anyValue = $mathExpression->getResult();
                    }
                    else
                        $anyValue =  $tToken->getProperty($strType);
                    $anyValue = $this->doModifier($anyValue, $tToken->getProperty('modifier'));
                    VariableStory::setVariable($tToken->getProperty('variable'), $anyValue);
                    break;
                case DTA_CONTINUE_LOOP:
                    switch($tToken->getType()){
                        case DTT_STMT_FOR:
                            $mdForID = DTS_STMT_FOR.md5($pTokens->currentIndex());
                            $intIndex = VariableStory::getVariable($mdForID.".index");
                            $intCount = VariableStory::getVariable($mdForID.".count");
                            $intNextPos = $tToken->getProperty('endfor');
                            $intIndex++;
                            $strVariable = $tToken->getProperty('variable');
                            if ($intCount>=$intIndex){
                                if ($strVariable!==NULL)
                                    VariableStory::setVariable($strVariable, $intIndex);
                                VariableStory::setVariable($mdForID.".index", $intIndex);
                            }
                            else{
                                VariableStory::delVariable($mdForID);
                                if ($strVariable!==NULL)
                                    VariableStory::delVariable($strVariable);
                                $pTokens->changeAction($pTokens->currentIndex(), DTA_STMT_DO);
                                $pTokens->changeAction($intNextPos, DTA_DO_NOTHING);
                                $pTokens->goToToken($intNextPos);
                            }
                            break;
                        case DTT_STMT_END_FOR:
                            $intNextPos = $tToken->getProperty('goto');
                            $pTokens->goToToken($intNextPos);
                            break;
                        case DTT_STMT_LOOP:
                            $strTableName = $tToken->getProperty('table');
                            $intNextPos = $tToken->getProperty('endloop');
                            $mdLoopID = $strTableName.".".DTS_STMT_LOOP.md5($pTokens->currentIndex());
                            $intIndex = VariableStory::getVariable($mdLoopID.".index");
                            $intIndex++;
                            $strKey = VariableStory::getVariable($mdLoopID.".keys".".".$intIndex);
                            if ($strKey!==NULL){
                                VariableStory::setVariable($mdLoopID.".index", $intIndex);
                                $arrValue = VariableStory::getVariable($strTableName.'.'.$strKey);
                                VariableStory::appendVariableTo($strTableName, $arrValue);
                            }
                            else{
                                $strKey = VariableStory::getVariable($mdLoopID.".keys.0");
                                $arrValue = VariableStory::getVariable($strTableName.'.'.$strKey);
                                $arrKeys = array_keys($arrValue);
                                foreach($arrKeys as $aKey)
                                    VariableStory::delVariable($strTableName.".".$aKey);
                                VariableStory::delVariable($mdLoopID);
                                $pTokens->changeAction($pTokens->currentIndex(), DTA_STMT_DO);
                                $pTokens->changeAction($intNextPos, DTA_DO_NOTHING);
                                $pTokens->goToToken($intNextPos);
                            }
                            break;
                        case DTT_STMT_END_LOOP:
                            $intNextPos = $tToken->getProperty('goto');
                            $pTokens->goToToken($intNextPos);
                            break;
                    }
                    break;
                case DTA_GOTO_END:
                    switch($tToken->getType()){
                        case DTT_STMT_ELSE:
                        case DTT_STMT_ELSE_IF:
                            $pTokens->changeAction($pTokens->currentIndex(), DTA_STMT_DO);
                            $intNextPos = $tToken->getProperty('endif');
                            $pTokens->goToToken($intNextPos);
                            break;
                    }
                    break;
                case DTA_STMT_DO:
                    switch($tToken->getType()){
                        case DTT_STMT_IF:
                        case DTT_STMT_ELSE_IF:
                            $strExpression = $tToken->getProperty('expression');
                            $strExpression = $this->parseExpression($strExpression);
                            $mathExpression = new MathExpression($strExpression);
                            $boolValue =  (boolean)$mathExpression->getResult();
                            $intNextPos = $tToken->getProperty('else');
                            if ($boolValue){
                                if ($intNextPos!==NULL)
                                    $pTokens->changeAction($intNextPos, DTA_GOTO_END);
                            }
                            else{
                                if ($intNextPos===NULL){
                                    $intNextPos = $tToken->getProperty('endif');
                                }
                                else
                                    $pTokens->changeAction($intNextPos, DTA_STMT_DO);
                                $pTokens->goToToken($intNextPos);
                            }
                            break;
                        case DTT_STMT_FOR:
                            $strType = $tToken->getProperty('type');
                            if ($strType=='var_count')
                                $intCount = VariableStory::getVariable($tToken->getProperty($strType));
                            elseif ($strType=='exp_count'){
                                $anyValue = $tToken->getProperty($strType);
                                $anyValue = $this->parseExpression($anyValue);
                                $mathExpression = new MathExpression($anyValue);
                                $intCount = $mathExpression->getResult();
                            }
                            else
                                $intCount =  $tToken->getProperty($strType);
                            $intNextPos = $tToken->getProperty('endfor');
                            if ($intCount>0){
                                $strVariable = $tToken->getProperty('variable');
                                if ($strVariable!==NULL)
                                    VariableStory::setVariable($strVariable, 1);
                                $mdForID = DTS_STMT_FOR.md5($pTokens->currentIndex());
                                VariableStory::setVariable($mdForID.".index", 1);
                                VariableStory::setVariable($mdForID.".count", $intCount);
                                $pTokens->changeAction($intNextPos, DTA_CONTINUE_LOOP);
                                $pTokens->changeAction($pTokens->currentIndex(), DTA_CONTINUE_LOOP);
                            }
                            else{
                                $pTokens->changeAction($intNextPos, DTA_DO_NOTHING);
                                $pTokens->goToToken($intNextPos);
                            }
                            break;
                        case DTT_STMT_LOAD:
                            $strFileLoaded = $tToken->getProperty('filename');
                            $interpretParser = new Interpreter($strFileLoaded);
                            $strInterpretCode .= $interpretParser->interpretOut();
                            break;
                        case DTT_STMT_LOOP:
                            $strTableName = ($tToken->getProperty('table'));
                            $intNextPos = $tToken->getProperty('endloop');
                            $arrValue = VariableStory::getVariable($strTableName);
                            $intCount = count($arrValue);
                            if (is_array($arrValue) && ($intCount>0)){
                                $mdLoopID = $strTableName.'.'.DTS_STMT_LOOP.md5($pTokens->currentIndex());
                                $intIndex = 0 ;
                                $arrKeys = array_keys($arrValue);
                                VariableStory::setVariable($mdLoopID.".index", $intIndex);
                                VariableStory::setVariable($mdLoopID.".keys", $arrKeys);
                                $arrValue = VariableStory::getVariable($strTableName.'.'.$arrKeys[$intIndex]);
                                VariableStory::appendVariableTo($strTableName, $arrValue);
                                $pTokens->changeAction($intNextPos, DTA_CONTINUE_LOOP);
                                $pTokens->changeAction($pTokens->currentIndex(), DTA_CONTINUE_LOOP);
                            }
                            else{
                                $pTokens->changeAction($pTokens->currentIndex(), DTA_STMT_DO);
                                $pTokens->changeAction($intNextPos, DTA_DO_NOTHING);
                                $pTokens->goToToken($intNextPos);
                            }
                            break;
                    }
                    break;
            }
        }
        normalizeHTMLWhiteSpaces($strInterpretCode);
        return $strInterpretCode;
    }
    public function parseExpression($strExpression){
        $strPattern = getCompleteValidVariablePattern();
        preg_match_all($strPattern, $strExpression, $arrMatches);
        foreach($arrMatches[0] as $strVar){
            $strVar = ltrim($strVar, DTS_VARIABLE);
            $anyValue = VariableStory::getVariable($strVar);
            $strPattern = "$".$strVar;
            $strExpression = str_ireplace($strPattern, $anyValue, $strExpression);
        }
        return $strExpression;
    }
    public function interpretOut(){
        return $this->parseCode();
    }
    private function doModifier($anyValue, $strModifiers){
        if (empty($strModifiers))
            return $anyValue;
        $anyChangeValue = $anyValue;
        $arrModifiers = stringSplitter($strModifiers, DTS_STRING_2.DTS_STRING_1, DTS_MODIFIER_SPLITTER);
        foreach($arrModifiers as $strModifier){
            $arrModifier = stringSplitter($strModifier, DTS_STRING_2.DTS_STRING_1, DTS_WHITESPACE);
            $intModifierCount = count($arrModifier);
            if ($intModifierCount<=0) continue;
            if (is_numeric($arrModifier[0])){
                $intStart = (double)$arrModifier[0];
                if (isset($arrModifier[1])){
                    if (is_numeric($arrModifier[1])){
                        $intLen = (double)$arrModifier[1];
                        $anyChangeValue = substr((string)$anyChangeValue, $intStart, $intLen);
                    }
                    else{
                        $anyChangeValue = substr((string)$anyChangeValue, $intStart);
                    }
                }
                else{
                    $anyChangeValue = substr((string)$anyChangeValue, $intStart);
                }
            }
            else{
                $arrModifier[0] = strtolower($arrModifier[0]);
                switch($arrModifier[0]){
                    case DTM_NUM_ABS:
                        $anyChangeValue = abs($anyChangeValue);
                        break;
                    case DTM_NUM_BINARY:
                        if (isset($arrModifier[1])){
                            if (is_numeric($arrModifier[1])){
                                $intBase = round((double)$arrModifier[1]);
                                $anyChangeValue = base_convert($anyChangeValue, 10, $intBase);
                            }
                            else
                                $anyChangeValue = sprintf("%b",$anyChangeValue);
                        }
                        else
                            $anyChangeValue = sprintf("%b",$anyChangeValue);
                        break;
                    case DTM_NUM_CHAR:
                        $anyChangeValue = sprintf("%c", $anyChangeValue);
                        break;
                    case DTM_NUM_STRING:
                        $anyChangeValue = sprintf("%s", $anyChangeValue);
                        break;
                    case DTM_NUM_NUMBER:
                        $anyChangeValue = (double)$anyChangeValue;
                        break;
                    case DTM_NUM_HEX_LOWER:
                        $anyChangeValue = sprintf("%x", $anyChangeValue);
                        break;
                    case DTM_NUM_HEX_UPPER:
                        $anyChangeValue = sprintf("%X", $anyChangeValue);
                        break;
                    case DTM_NUM_OCTAL:
                        $anyChangeValue = sprintf("%o", $anyChangeValue);
                        break;
                    case DTM_NUM_ROUND:
                        if (isset($arrModifier[1])){
                             $anyChangeValue = round((double)$anyChangeValue, (int)$arrModifier[1]);
                            $anyChangeValue =  sprintf("%.".(int)$arrModifier[1]."f", $anyChangeValue );
                        }
                        else{
                            $anyChangeValue = round((double)$anyChangeValue, 2);
                            $anyChangeValue =  sprintf("%.2f", $anyChangeValue );
                        }
                        break;
                    case DTM_STR_BREAK_LINE:
                        $anyChangeValue .= "<br>";
                        break;
                    case DTM_STR_CRC:
                    case DTM_STR_CRC32:
                        $anyChangeValue = crc32($anyChangeValue);
                        break;
                    case DTM_STR_FIRST_CASE:
                        $anyChangeValue = ucfirst($anyChangeValue);
                        break;
                    case DTM_STR_LOWER_CASE:
                        $anyChangeValue = strtolower($anyChangeValue);
                        break;
                    case DTM_STR_UPPER_CASE:
                        $anyChangeValue = strtoupper($anyChangeValue);
                        break;
                    case DTM_STR_WORD_CASE:
                        $anyChangeValue = ucwords($anyChangeValue);
                        break;
                    case DTM_STR_MD5:
                        $anyChangeValue = md5($anyChangeValue);
                        break;
                    case DTM_STR_SH1:
                        $anyChangeValue = sha1($anyChangeValue);
                        break;
                    case DTM_STR_WORD_WRAP:
                        if (isset($arrModifier[1]))
                            $intWidth = (double)$arrModifier[1];
                        else $intWidth = 75;
                        $strBreak = "<br>";
                        $strEndString = '';
                        if (isset($arrModifier[2])){
                            if (stringClosedBy($arrModifier[2], DTS_STRING_1.DTS_STRING_2))
                                $strEndString = stringTrimOnce2a($arrModifier[2], $arrModifier[2][0]);
                            else
                                $strEndString = "<br>";
                        }
                        $anyChangeValue = wordwrap($anyChangeValue, $intWidth, $strBreak, TRUE);
                        $anyChangeValue .= $strEndString;
                        break;
                    case DTM_STR_REPEAT :
                        if (isset($arrModifier[1]))
                            $intCount = (double)$arrModifier[1];
                        else $intCount = 0;
                        if (isset($arrModifier[2])){
                            if (stringClosedBy($arrModifier[2], DTS_STRING_1.DTS_STRING_2))
                                $strRepeat = stringTrimOnce2a($arrModifier[2],  $arrModifier[2][0]);
                            else
                                $strRepeat = $anyChangeValue;
                        }
                        else $strRepeat = $anyChangeValue;
                        $anyChangeValue .= str_repeat($strRepeat, $intCount);
                        break;
                    case DTM_STR_PAD_RIGHT:
                    case DTM_STR_PAD_LEFT:
                    case DTM_STR_PAD_BOTH:
                        if (isset($arrModifier[1]))
                            $intWidth = (double)$arrModifier[1];
                        else $intWidth = strlen($anyChangeValue);
                        if (isset($arrModifier[2])){
                            if (stringClosedBy($arrModifier[2], DTS_STRING_1.DTS_STRING_2)){
                                $strPadding = stringTrimOnce2a($arrModifier[2], $arrModifier[2][0]);
                                if ($strPadding == '')
                                    $strPadding = " ";
                            }
                            else
                                $strPadding = " ";
                        }
                        else $strPadding = " ";
                        if ($arrModifier[0]==DTM_STR_PAD_RIGHT) $intDirection = STR_PAD_RIGHT;
                        elseif ($arrModifier[0]==DTM_STR_PAD_LEFT) $intDirection = STR_PAD_LEFT;
                        else $intDirection = STR_PAD_BOTH;
                        $anyChangeValue = str_pad($anyChangeValue, $intWidth,$strPadding, $intDirection);
                        break;
                    case DTM_STR_STRIP_TAGS:
                        $anyChangeValue = strip_tags($anyChangeValue);
                        break;
                    case DTM_STR_STRIP_QUOTE_C_STYLE:
                        $anyChangeValue = addcslashes($anyChangeValue,"'\"");
                        break;
                    case DTM_STR_STRIP_UNQUOTE_C_STYLE:
                        $anyChangeValue = stripcslashes($anyChangeValue);
                        break;
                    case DTM_STR_STRIP_QUOTE:
                        $anyChangeValue = addslashes($anyChangeValue);
                        break;
                    case DTM_STR_STRIP_UNQUOTE:
                        $anyChangeValue = stripslashes($anyChangeValue);
                        break;
                }
            }
        }
        return $anyChangeValue;
    }
    private function haveTags($strString){
        $strTagPattern = '/<[\/]?([^>])*>/';
        return preg_match($strTagPattern, $strString);
    }
}
