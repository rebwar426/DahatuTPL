<?php
/**
 * Created by PhpStorm.
 * User: EbdulRahman
 * Date: 4/26/14
 * Time: 7:03 PM
 */

final class Configuration {

    public function dahatuInfo(){
        $arrInfo = array();
        $arrInfo['name'] = 'DahatuTPL';
        $arrInfo['version'] = '1.0.0';
        $arrInfo['nuser'] = 'EbdulRehman Demya';
        $arrInfo['programmer'] = 'AbdolRahman Damia';
        $arrInfo['about'] = '';
        $arrInfo['copyright'] = '';

        return $arrInfo;
    }

    private static $DAHATU_ROOT = NULL;
    public static function setDahatuRoot($strPath){
        if (is_dir($strPath))
            self::$DAHATU_ROOT = $strPath;
        else
            self::$DAHATU_ROOT = NULL;
    }
    public static function getDahatuRoot(){
        return self::$DAHATU_ROOT;
    }

    private static $TPL_FILES_PATH = NULL;
    public static function setTPLFilesPath($tplFilesPath){
        if (is_dir($tplFilesPath))
            self::$TPL_FILES_PATH = $tplFilesPath;
        else
            self::$TPL_FILES_PATH = NULL;
    }
    public static function getTPLFilesPath(){
        return self::$TPL_FILES_PATH;
    }
} 