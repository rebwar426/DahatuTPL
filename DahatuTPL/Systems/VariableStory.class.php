<?php
/**
 * Created by PhpStorm.
 * User: EbdulRahman
 * Date: 5/1/14
 * Time: 12:35 PM
 */

require_once("Libraries/TokenType.lib.php");
require_once("Libraries/TokenSymbol.lib.php");
require_once("Libraries/TokenRegex.lib.php");

final class VariableStory {
    private static $arrVariableStore = array();

    public static function setVariable($strVarName, $anyVarValue){
        if (!isValidVariable($strVarName))
            return;
        if (is_array($anyVarValue)){
            if (!self::checkArrayVarNames($anyVarValue))
                return;
        }
        $arrVars = explode(DTS_VARIABLE_STICK, $strVarName);
        $intVarCount = count($arrVars);
        $tmpVars = &self::$arrVariableStore;
        for($index=0; $index<$intVarCount; $index++){
            if ((!isset($tmpVars[$arrVars[$index]])) || (is_scalar($tmpVars[$arrVars[$index]])))
                $tmpVars[$arrVars[$index]] = array();
            $tmpVars = &$tmpVars[$arrVars[$index]];
        }
        $tmpVars = $anyVarValue;
    }
    public static function appendVariableTo($strVariableName,array $arrValues){
        $arrVariableName = explode(DTS_VARIABLE_STICK, $strVariableName);
        $tempVariable = self::$arrVariableStore;
        foreach ($arrVariableName as $strVariable)
            if (!isset($tempVariable[$strVariable]))
                return;
            else
                $tempVariable = $tempVariable[$strVariable];
        foreach($arrValues as $strKey=>$anyValue)
            VariableStory::setVariable($strVariableName.".".$strKey, $anyValue);
    }
    public static function getVariable($strVarName){
        if (!isValidVariable($strVarName))
            return NULL;
        $arrVars = explode(DTS_VARIABLE_STICK, $strVarName);
        $intVarCount = count($arrVars);
        $tmpVars = &self::$arrVariableStore;
        for($index=0; $index<$intVarCount; $index++){
            if (isset($tmpVars[$arrVars[$index]])){
                $tmpVars = &$tmpVars[$arrVars[$index]];
            }
            else
                return NULL;
        }
        return $tmpVars;
    }
    public static function delVariable($strVarName){
        if (!isValidVariable($strVarName))
            return NULL;
        $arrVars = explode(DTS_VARIABLE_STICK, $strVarName);
        $intVarCount = count($arrVars);
        $tmpVars = &self::$arrVariableStore;
        for($index=0; $index<$intVarCount-1; $index++){
            $strIndex = $arrVars[$index];
            if (is_array($tmpVars) && (isset($tmpVars[$strIndex])))
                $tmpVars = &$tmpVars[$strIndex];
            else
                return NULL;
        }
        if (isset($tmpVars[$arrVars[$intVarCount-1]]))
            unset($tmpVars[$arrVars[$intVarCount-1]]);
    }
    public static function setValueFromJSON($strJSONValue, $strJSONName=''){
        $jsonArray = json_decode($strJSONValue,true);
        if (!is_null($jsonArray)){
            if ($strJSONName!='')
                self::setVariable($strJSONName, $jsonArray);
            else{
                foreach($jsonArray as $iKey=>$iVal)
                    self::setVariable($iKey, $iVal);
            }
        }
    }
    public static function getValueByJSON($strVarName=''){
        if ((!isValidVariable($strVarName))&&($strVarName!=''))
            return NULL;
        $tmpVars = &self::$arrVariableStore;
        if ($strVarName!=''){
            $arrVars = explode(DTS_VARIABLE_STICK, $strVarName);
            $intVarCount = count($arrVars);
            for($index=0; $index<$intVarCount; $index++){
                if (isset($tmpVars[$arrVars[$index]])){
                    $tmpVars = &$tmpVars[$arrVars[$index]];
                }
                else
                    return NULL;
            }
        }
        return json_encode($tmpVars);
    }
    public static function showVariables(){
        foreach(self::$arrVariableStore as $vKey=>$vVal){
            if (is_scalar($vVal)){
                $vKey = str_pad($vKey, 30," ", STR_PAD_RIGHT);
                $vVal = str_pad($vVal, 30," ", STR_PAD_RIGHT);
                echo $vKey,": " ,$vVal, PHP_EOL;
            }
            elseif(is_array($vVal)){
                $vVal = self::arrayRecursive($vVal, $vKey.DTS_VARIABLE_STICK);
                echo $vVal;
            }
        }
    }
    private static function arrayRecursive($arrRecursive, $strName){
        $strReturn = '';
        foreach($arrRecursive as $vKey=>$vVal){
            $vKey = $strName.$vKey;
            if (is_scalar($vVal)){
                $vKey = str_pad($vKey, 30," ", STR_PAD_RIGHT);
                $vVal = str_pad($vVal, 30," ", STR_PAD_RIGHT);
                $strReturn .= $vKey.": " .$vVal. PHP_EOL;
            }
            elseif(is_array($vVal)){
                $strReturn .= self::arrayRecursive($vVal, $vKey.DTS_VARIABLE_STICK);
            }
        }
        return $strReturn;
    }
    private static function checkArrayVarNames(array $arrData){
        if (empty($arrData)) return FALSE;
        $isValid = TRUE;
        foreach($arrData as $vkey=>$vVal){
            if(!isValidVariable("variable.".$vkey)){
                $isValid  = FALSE;
                return $isValid;
            }
            if (is_array($vVal))
                $isValid &= self::checkArrayVarNames($vVal);
            if(!$isValid)
                return $isValid;
        }
        return $isValid;
    }
}
