<?php
/**
 * Created by PhpStorm.
 * User: EbdulRahman
 * Date: 4/27/14
 * Time: 6:33 PM
 */

class Stack {
    private $arrStackStory;
    private $intTopPointer;
    
    public function Stack(){
        $this->clearItems();
    }
    public function pushItem($anyObject){
        $this->intTopPointer++;
        $this->arrStackStory[$this->intTopPointer-1] = $anyObject;
    }
    public function clearItems(){
        $this->arrStackStory = array();
        $this->intTopPointer = 0;
    }
    public function popItem($boolRemoveItems = true){
        if ($this->intTopPointer<1) return NULL;
        $this->intTopPointer--;
        $anyLastItem = $this->arrStackStory[$this->intTopPointer];
        if ($boolRemoveItems)
            unset($this->arrStackStory[$this->intTopPointer]);
        else
            $this->intTopPointer++;
        return $anyLastItem;
    }
    public function isEmpty(){
        return ($this->intTopPointer<1)?TRUE:FALSE;
    }
    public function countItems(){
        return $this->intTopPointer;
    }
    private function echoItems($anyItems,$perfixChar=''){
        if($perfixChar=='') $perfixChar='-';
        foreach($anyItems as $valItem){
            if (is_array($valItem)){
                $this->echoItems($valItem,$perfixChar.$perfixChar[0]);
            }
            elseif (is_object($valItem))
                echo $perfixChar.get_class($valItem)."\n";
            else
                echo $perfixChar.$valItem."\n";
        }
    }
    public function show(){
        echo "\n***********************************************************";
        $this->echoItems($this->arrStackStory,'.');
        echo "\n***********************************************************\n";
    }
}
