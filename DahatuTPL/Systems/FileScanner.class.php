<?php
/**
 * Created by PhpStorm.
 * User: EbdulRahman
 * Date: 4/27/14
 * Time: 11:00 PM
 */

require_once("Libraries/TokenSymbol.lib.php");

class FileScanner {
    private $intPosIndex = -1;
    private $strFileContent = NULL;
    private $intLenContent = 0;

    public function FileScanner($strFilePath=''){
        $this->loadFileContent($strFilePath);
    }
    public function loadFileContent($strFilePath){
        $this->resetScanner();
        if (!file_exists($strFilePath) || ($strFilePath==''))
            return;
        try{
            $hFile = fopen($strFilePath, "r");
            if (!$hFile)
                return;
            $this->strFileContent = fread($hFile, filesize($strFilePath));
            $this->intLenContent = strlen($this->strFileContent);
            fclose($hFile);
        }
        catch (Exception $exError){
            die("<p style='color: red'>Error : {$exError->getMessage()}</p>");
        }
    }
    public function sourceCode($strSourceCode){
        $this->resetScanner();
        $this->strFileContent=$strSourceCode;
        $this->intLenContent = strlen($this->strFileContent);
    }
    public function getChar(){
        if ($this->strFileContent == NULL)
            return NULL;
        //do{
            if ($this->intLenContent<=$this->intPosIndex+1)
                return NULL;
            $this->intPosIndex++;
            $char = $this->strFileContent[$this->intPosIndex];
        //}while(isNewLineSign($char));
        return $char;
    }
    public function getNextChar(){
        if ($this->strFileContent == NULL)
            return NULL;
        if ($this->intLenContent<=$this->intPosIndex+1)
            return NULL;
        $char = $this->strFileContent[$this->intPosIndex+1];
        return $char;
    }
    public function getCurChar(){
        if ($this->strFileContent == NULL)
            return NULL;
        if ($this->intLenContent<$this->intPosIndex+1)
            return NULL;
        $char=$this->strFileContent[$this->intPosIndex];
        return $char;
    }
    public function goBackAChar(){
        $this->intPosIndex--;
        if ($this->intPosIndex<-1) $this->intPosIndex=-1;
    }
    public function skipWhiteSpaces(){
        $intSkipCount = -1;
        do{
            $intSkipCount++;
            $char = $this->getChar();
        }while(isWhitespace($char));
        if ($char!==NULL)
            $this->goBackAChar();
        return $intSkipCount;
    }
    public function endOfFile(){
        if ($this->intLenContent>$this->intPosIndex+1) return FALSE;
        return TRUE;
    }
    public function showFileContent(){
        return $this->strFileContent;
    }
    public function resetScanner(){
        $this->intPosIndex = -1;
        $this->strFileContent = NULL;
        $this->intLenContent = 0;
    }
}
