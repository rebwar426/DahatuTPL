<?php
/**
 * Created by EbdulRahman Demya
 * User: Home
 * Date: 2/25/14
 * Time: 11:26 PM
 */

define('DTM_STR_UPPER_CASE','uc');
define('DTM_STR_LOWER_CASE','lc');
define('DTM_STR_FIRST_CASE','uf');
define('DTM_STR_WORD_CASE','uw');
define('DTM_STR_CRC32','crc32');
define('DTM_STR_CRC','crc');
define('DTM_STR_MD5','md5');
define('DTM_STR_SH1','sh1');
define('DTM_STR_PAD_LEFT','pl');
define('DTM_STR_PAD_RIGHT','pr');
define('DTM_STR_PAD_BOTH','pb');
define('DTM_STR_REPEAT','rpt');
define('DTM_STR_BREAK_LINE','br');
define('DTM_STR_WORD_WRAP','ww');
define('DTM_STR_STRIP_TAGS','st');
define('DTM_STR_STRIP_QUOTE_C_STYLE','qcs');
define('DTM_STR_STRIP_UNQUOTE_C_STYLE','uqcs');
define('DTM_STR_STRIP_QUOTE','ss');
define('DTM_STR_STRIP_UNQUOTE','uss');


define('DTM_NUM_BINARY','b');
define('DTM_NUM_CHAR','chr');
define('DTM_NUM_STRING','str');
define('DTM_NUM_NUMBER','num');
define('DTM_NUM_ABS','abs');
define('DTM_NUM_ROUND','rnd');
define('DTM_NUM_OCTAL','oct');
define('DTM_NUM_HEX_LOWER','lhex');
define('DTM_NUM_HEX_UPPER','uhex');

