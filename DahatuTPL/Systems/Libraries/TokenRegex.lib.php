<?php
/**
 * Created by PhpStorm.
 * User: EbdulRahman
 * Date: 5/1/14
 * Time: 1:42 PM
 */

$strRegex = array(
    DTT_VARIABLE => "/^[_]*[a-z][a-z0-9_]*([.](([_]*[a-z][a-z0-9_]*)|([0-9]+)))*$/is",
    DTT_EXPRESSION => "/^[(]*([!]?[-+]?(([0-9]+([.][0-9]*)?)|([$][_]*[a-z][a-z0-9_]*([.](([_]*[a-z][a-z0-9_]*)|([0-9]+)))*)))([)]*[\\/\*%\-\+<>=&\^\|#][\(]*([!]?[-+]?(([0-9]+([.][0-9]*)?)|([$][_]*[a-z][a-z0-9_]*([.](([_]*[a-z][a-z0-9_]*)|([0-9]+)))*))))*[\)]*$/is",
    DTT_STMTS=>"/^[a-z]+[0-9a-z]*$/is"
);

function isValidVariable($strVarName){
    global $strRegex;
    if (preg_match($strRegex[DTT_VARIABLE],$strVarName)) return TRUE;
    return FALSE;
}
function isValidExpression($strExpVal){
    global $strRegex;
    if (preg_match($strRegex[DTT_EXPRESSION],$strExpVal)) return TRUE;
    return FALSE;
}
function isValidStatement($strStatement){
    global $strRegex;
    if (preg_match($strRegex[DTT_STMTS],$strStatement)) return TRUE;
    return FALSE;
}
function getCompleteValidVariablePattern(){
    global $strRegex;
    $strPattern = $strRegex[DTT_VARIABLE];
    $strPattern = str_replace("$", "", $strPattern);
    $strPattern = str_replace("^", "[$]", $strPattern);
    return $strPattern;
}

