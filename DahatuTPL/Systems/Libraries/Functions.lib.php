<?php
/**
 * Created by PhpStorm.
 * User: EbdulRahman
 * Date: 4/29/14
 * Time: 6:09 PM
 */

function stringTrimOnce2a($strString, $stringCharSign){
    if ($strString=='') return $strString;
    $intStart = 0;
    if ($strString[$intStart]==$stringCharSign)
        $intStart++;
    $intEnd = strlen($strString);
    if ($strString[$intEnd-1]==$stringCharSign)
        $intEnd--;
    return substr($strString, $intStart, $intEnd - $intStart);
}
function stringTrimOnce3a($strString, $strBeginSign, $strEndSign){
    if ($strString=='') return $strString;
    $intStart = 0;
    if ($strString[$intStart]==$strBeginSign)
        $intStart++;
    $intEnd = strlen($strString);
    if ($strString[$intEnd-1]==$strEndSign)
        $intEnd--;
    return substr($strString, $intStart, $intEnd - $intStart);
}
function stringCleanSubStr($strString, $strSub, $strCleanBy){
    $pattern = "/".$strSub."/";
    return preg_replace($pattern, $strCleanBy, $strString);
}
function stringCleanChars($strString, $strList, $strCleanBy){
    $pattern = "/[".$strList."]/";
    return preg_replace($pattern, $strCleanBy, $strString);
}
function stringSplitter($strString, $strIgnoreList, $strCharSplitBy=' '){
    $arrSplit = array();
    $strSplit = '';
    $strSign = '';
    for($index=0; $index<strlen($strString); $index++){
        $ch = $strString[$index];
        if (is_numeric(strpos($strIgnoreList, $ch))&&($strSign=='')){
            $strSign = $ch;
            $strSplit .= $ch;
        }
        elseif (is_numeric(strpos($strIgnoreList, $ch))&&($strSign!='')){
            $strSign = '';
            $strSplit .= $ch;
        }
        elseif ((is_numeric(strpos($strCharSplitBy, $ch))&&($strSign==''))){
            $strSplit = trim($strSplit);
            if ($strSplit!=''){
                $arrSplit[]= $strSplit;
                $strSplit = '';
            }
        }
        else
            $strSplit .= $ch;
    }
    if (trim($strSplit)!='')
        $arrSplit[] = $strSplit;
    return $arrSplit;
}
function stringClosedBy($strString, $strCloserList){
    $intStr = strlen($strString);
    if ($intStr<=1) return FALSE;
    if(is_bool(strpos($strCloserList,$strString[0]))) return FALSE;
    $strSign = $strString[0];
    if($strSign != $strString[$intStr-1]) return FALSE;
    return TRUE;
}