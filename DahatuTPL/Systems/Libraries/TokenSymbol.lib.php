<?php
/**
 * Created by PhpStorm.
 * User: EbdulRahman
 * Date: 4/29/14
 * Time: 4:13 PM
 */

define('DTS_TAG_START', '<');
define('DTS_TAG_END', '>');
define('DTS_WHITESPACE', " \t\r\n");
define('DTS_NEW_LINE_SIGN', "\r\n");
define('DTS_VARIABLE', '$');
define('DTS_VARIABLE_STICK', '.');
define('DTS_MODIFIER', '@');
define('DTS_MODIFIER_SPLITTER', ',');
define('DTS_EXPRESSION_START', '(');
define('DTS_EXPRESSION_END', ')');
define('DTS_STRING_1', '"');
define('DTS_STRING_2', "'");
define('DTS_STMT_IF', 'IF');
define('DTS_STMT_ELSE', 'ELSE');
define('DTS_STMT_END', '/');
define('DTS_STMT_LOAD', 'LOAD');
define('DTS_STMT_LOOP', 'LOOP');
define('DTS_STMT_FOR', 'FOR');
define('DTS_NUM_SIGN_PLUS', '+');
define('DTS_NUM_SIGN_MINUS', '-');
define('DTS_COMMENT', '*');

function isWhitespace($strChar){
    if (is_numeric(strpos(DTS_WHITESPACE,$strChar))) return TRUE;
    return FALSE;
}
function isNewLineSign($strChar){
    if (is_numeric(strpos(DTS_NEW_LINE_SIGN,$strChar))) return TRUE;
    return FALSE;
}
function normalizeHTMLWhiteSpaces(&$strHTMLSource){
    //$strPattern = "/[\r\n]+/";
    $strHTMLSource = trim($strHTMLSource);
    $strHTMLSource = preg_replace('~>\s+<~m', '><', $strHTMLSource);
    //$strHTMLSource = preg_replace($strPattern,' ', $strHTMLSource);
}
