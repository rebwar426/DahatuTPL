<?php
/**
 * Created by PhpStorm.
 * User: EbdulRahman
 * Date: 4/27/14
 * Time: 10:00 PM
 */

define('DTA_DO_NOTHING',        0);
define('DTA_PRINT_OUT',         1);
define('DTA_ASSIGN',            2);
define('DTA_STMT_DO',           3);
define('DTA_GOTO_END',          4);
define('DTA_CONTINUE_LOOP',     5);

function getTokenActionByName($intAction){
    $strAction = '';
    switch($intAction){
        case DTA_DO_NOTHING:
            $strAction = 'DTA_DO_NOTHING';
            break;
        case DTA_PRINT_OUT:
            $strAction = 'DTA_PRINT_OUT';
            break;
        case DTA_ASSIGN:
            $strAction = 'DTA_ASSIGN';
            break;
        case DTA_STMT_DO:
            $strAction = 'DTA_STMT_DO';
            break;
        case DTA_GOTO_END:
            $strAction = 'DTA_GOTO_END';
            break;
        case DTA_CONTINUE_LOOP:
            $strAction = 'DTA_CONTINUE_LOOP';
            break;
        default:
            $strAction = 'DTA_DO_NOTHING';
    }
    return $strAction;
}
