<?php
/**
 * Created by AbdulRahman Damia.
 * User: Home
 * Date: 1/28/14
 * Time: 10:02 PM
 */

define('DOT_PARENTHESES_OPEN', 21);
define('DOT_PARENTHESES_CLOSE', 22);
define('DOT_OPT_MINUS', 23);
define('DOT_OPT_PLUS', 24);
define('DOT_OPT_POWER', 25);
define('DOT_OPT_NOT', 26);
define('DOT_OPT_MUL', 27);
define('DOT_OPT_REAL_DIV', 28);
define('DOT_OPT_INT_DIV', 29);
define('DOT_OPT_MOD', 30);
define('DOT_OPT_AND', 31);
define('DOT_OPT_ADD', 32);
define('DOT_OPT_SUB', 33);
define('DOT_OPT_OR', 34);
define('DOT_OPT_MORE', 35);
define('DOT_OPT_MORE_EQUAL', 36);
define('DOT_OPT_EQUAL', 37);
define('DOT_OPT_NOT_EQUAL', 38);
define('DOT_OPT_LESS', 39);
define('DOT_OPT_LESS_EQUAL', 40);

$operatorPrecedence = array();
$operatorPrecedence[DOT_PARENTHESES_OPEN]   =   array('pre'=> '0', 'opt'=> '(', 'name'=>'BEGIN'     );
$operatorPrecedence[DOT_PARENTHESES_CLOSE]  =   array('pre'=> '0', 'opt'=> ')', 'name'=>'END'       );
$operatorPrecedence[DOT_OPT_MINUS]          =   array('pre'=>'51', 'opt'=>'/-', 'name'=>'MINUS'     );
$operatorPrecedence[DOT_OPT_PLUS]           =   array('pre'=>'51', 'opt'=>'/+', 'name'=>'PLUS'      );
$operatorPrecedence[DOT_OPT_POWER]          =   array('pre'=>'42', 'opt'=> '^', 'name'=>'POWER'     );
$operatorPrecedence[DOT_OPT_NOT]            =   array('pre'=>'33', 'opt'=> '!', 'name'=>'NOT'       );
$operatorPrecedence[DOT_OPT_MUL]            =   array('pre'=>'24', 'opt'=> '*', 'name'=>'MULTIPLY'  );
$operatorPrecedence[DOT_OPT_REAL_DIV]       =   array('pre'=>'24', 'opt'=> '/', 'name'=>'REAL_DIV'  );
$operatorPrecedence[DOT_OPT_INT_DIV]        =   array('pre'=>'24', 'opt'=>"\\", 'name'=>'INT_DIV'   );
$operatorPrecedence[DOT_OPT_MOD]            =   array('pre'=>'24', 'opt'=> '%', 'name'=>'MOD'       );
$operatorPrecedence[DOT_OPT_AND]            =   array('pre'=>'24', 'opt'=> '&', 'name'=>'AND'       );
$operatorPrecedence[DOT_OPT_SUB]            =   array('pre'=>'15', 'opt'=> '-', 'name'=>'SUBTRACT'  );
$operatorPrecedence[DOT_OPT_ADD]            =   array('pre'=>'15', 'opt'=> '+', 'name'=>'ADD'       );
$operatorPrecedence[DOT_OPT_OR]             =   array('pre'=>'15', 'opt'=> '|', 'name'=>'OR'        );
$operatorPrecedence[DOT_OPT_LESS]           =   array('pre'=>'11', 'opt'=> '<', 'name'=>'LESS'      );
$operatorPrecedence[DOT_OPT_LESS_EQUAL]     =   array('pre'=>'11', 'opt'=>'<=', 'name'=>'LESS_EQUAL');
$operatorPrecedence[DOT_OPT_EQUAL]          =   array('pre'=>'11', 'opt'=> '=', 'name'=>'EQUAL'     );
$operatorPrecedence[DOT_OPT_NOT_EQUAL]      =   array('pre'=>'11', 'opt'=> '#', 'name'=>'UNEQUAL'   );
$operatorPrecedence[DOT_OPT_MORE]           =   array('pre'=>'11', 'opt'=> '>', 'name'=>'MORE'      );
$operatorPrecedence[DOT_OPT_MORE_EQUAL]     =   array('pre'=>'11', 'opt'=>'>=', 'name'=>'MORE_EQUAL');


function getPrecedence($strOptName){
    global $operatorPrecedence;
    if (is_null($strOptName)) return 0;
    foreach($operatorPrecedence as $optPrecedence)
        if ( strcmp($strOptName,$optPrecedence['name']) == 0)
            return $optPrecedence['pre'];
    return 0;
}
function getOperatorName($optCharSign){
    global $operatorPrecedence;
    foreach($operatorPrecedence as $optPrecedence)
        if ( strcmp($optCharSign,$optPrecedence['opt']) == 0)
            return $optPrecedence['name'];
    return NULL;
}
function getOperator($DOType){
    global $operatorPrecedence;
    return $operatorPrecedence[$DOType]['opt'];
}
function getOperatorNameBDOT($DOType){
    global $operatorPrecedence;
    return $operatorPrecedence[$DOType]['name'];
}
function isOperator($optName){
    global $operatorPrecedence;
    foreach($operatorPrecedence as $optItem)
        if ( strcmp($optName,$optItem['action']) == 0)
            return true;
    return false;
}
function isOperand($operandChars){
    global $operatorPrecedence;
    $digitList='1234567890.';
    if (is_bool(strpos($digitList,$operandChars))) return false;
    return true;
}
function getDOTOperatorBYName($optName){
    global $operatorPrecedence;
    switch($optName){
        case $operatorPrecedence[DOT_PARENTHESES_OPEN]['name']:
            return DOT_PARENTHESES_OPEN;
            break;
        case $operatorPrecedence[DOT_PARENTHESES_CLOSE]['name']:
            return DOT_PARENTHESES_CLOSE;
            break;
        case $operatorPrecedence[DOT_OPT_MINUS]['name']:
            return DOT_OPT_MINUS;
            break;
        case $operatorPrecedence[DOT_OPT_PLUS]['name']:
            return DOT_OPT_PLUS;
            break;
        case $operatorPrecedence[DOT_OPT_POWER]['name']:
            return DOT_OPT_POWER;
            break;
        case $operatorPrecedence[DOT_OPT_NOT]['name']:
            return DOT_OPT_NOT;
            break;
        case $operatorPrecedence[DOT_OPT_MUL]['name']:
            return DOT_OPT_MUL;
            break;
        case $operatorPrecedence[DOT_OPT_REAL_DIV]['name']:
            return DOT_OPT_REAL_DIV;
            break;
        case $operatorPrecedence[DOT_OPT_INT_DIV]['name']:
            return DOT_OPT_INT_DIV;
            break;
        case $operatorPrecedence[DOT_OPT_MOD]['name']:
            return DOT_OPT_MOD;
            break;
        case $operatorPrecedence[DOT_OPT_AND]['name']:
            return DOT_OPT_AND;
            break;
        case $operatorPrecedence[DOT_OPT_SUB]['name']:
            return DOT_OPT_SUB;
            break;
        case $operatorPrecedence[DOT_OPT_ADD]['name']:
            return DOT_OPT_ADD;
            break;
        case $operatorPrecedence[DOT_OPT_OR]['name']:
            return DOT_OPT_OR;
            break;
        case $operatorPrecedence[DOT_OPT_LESS]['name']:
            return DOT_OPT_LESS;
            break;
        case $operatorPrecedence[DOT_OPT_LESS_EQUAL]['name']:
            return DOT_OPT_LESS_EQUAL;
            break;
        case $operatorPrecedence[DOT_OPT_EQUAL]['name']:
            return DOT_OPT_EQUAL;
            break;
        case $operatorPrecedence[DOT_OPT_NOT_EQUAL]['name']:
            return DOT_OPT_NOT_EQUAL;
            break;
        case $operatorPrecedence[DOT_OPT_MORE_EQUAL]['name']:
            return DOT_OPT_MORE_EQUAL;
            break;
        case $operatorPrecedence[DOT_OPT_MORE]['name']:
            return DOT_OPT_MORE;
            break;
    }
    return 0;
}
function isLogicOperator($optName){
    switch(getDOTOperatorBYName($optName)){
        case DOT_OPT_LESS:
        case DOT_OPT_LESS_EQUAL:
        case DOT_OPT_EQUAL:
        case DOT_OPT_NOT_EQUAL:
        case DOT_OPT_MORE:
        case DOT_OPT_MORE_EQUAL:
            $boolLogic = true;
            break;
        default:
            $boolLogic=false;
    }
    return $boolLogic;
}