<?php
/**
 * Created by PhpStorm.
 * User: EbdulRahman
 * Date: 4/27/14
 * Time: 9:48 PM
 */

require_once("Includes/Stack.class.php");
require_once("FileScanner.class.php");
require_once("Libraries/TokenType.lib.php");
require_once("Libraries/TokenAction.lib.php");
require_once("Libraries/TokenSymbol.lib.php");
require_once("Libraries/TokenRegex.lib.php");
require_once("Libraries/Functions.lib.php");

final class Token {
    private $intType = DTT_NO_TYPE;
    private $intAction = DTA_DO_NOTHING;
    private $arrPropertyStore = array();

    public function Token($dttType=DTT_HTML_TAG, $dtaAction = DTA_PRINT_OUT ,array $arrProperties=array()){
        $this->setToken($dttType, $dtaAction, $arrProperties);
    }
    public function setToken($dttType, $dtaAction,array $arrProperties=array()){
        $this->intType = $dttType;
        $this->intAction = $dtaAction;
        $this->arrPropertyStore = $arrProperties;
    }
    public function __toString(){
        $strAction = getTokenActionByName($this->intAction);
        $strType = getTokenTypeByName($this->intType);
        $strExtraData = '';
        foreach($this->arrPropertyStore as $adField=>$adValue)
            $strExtraData .= $adField."=".$adValue.", ";
        $strExtraData = rtrim($strExtraData, ", ");
        return str_pad($strType, 20, " ", STR_PAD_RIGHT) . ' : ' . str_pad($strAction, 20, " ", STR_PAD_RIGHT) . ' : ' . $strExtraData;
    }
    public function getType(){
        return $this->intType;
    }
    public function getAction(){
        return $this->intAction;
    }
    public function setAction($dtaAction){
        $this->intAction = $dtaAction;
    }
    public function setType($dttType){
        $this->intType = $dttType;
    }
    public function setProperty($strFieldName, $anyValue){
        $this->arrPropertyStore[$strFieldName] = $anyValue;
    }
    public function getProperty($strFieldName){
        if (isset($this->arrPropertyStore[$strFieldName]))
            return $this->arrPropertyStore[$strFieldName];
        return NULL;
    }
    public function setProperties(array $arrProperties=array()){
        $this->arrPropertyStore = $arrProperties;
    }
    public function getProperties(){
        return $this->arrPropertyStore;
    }
}


final class Tokens{
    private $arrTokens = array();
    private $intIndex = -1;
    private $fileScanner = NULL;

    public function Tokens($strSourceCode, $boolFilePath=TRUE){
        $this->fileScanner = new FileScanner();
        if ($boolFilePath)
            $this->fileScanner->loadFileContent($strSourceCode);
        else
            $this->fileScanner->sourceCode($strSourceCode);
        $this->Tokenizer();
    }
    private function Tokenizer(){
        $this->arrTokens = array();
        $this->intIndex = -1;
        $tmpScanner = &$this->fileScanner;
        do{
            $tToken = new Token();
            $ch = $tmpScanner->getChar();
            $strTagTokens = $ch;
            if ($ch==DTS_TAG_START){
                $intSkipped = $tmpScanner->skipWhiteSpaces();
                $strTagTokens .= str_repeat(' ', $intSkipped);
                $ch = $tmpScanner->getChar();
                if (ctype_punct($ch)){
                    switch($ch){
                        case DTS_VARIABLE:
                            $tToken = new Token(DTT_VARIABLE, DTA_PRINT_OUT);
                            $strVariableName = '';
                            $boolCanOut = FALSE;
                            do{
                                $strVariableName .= $ch;
                                $strTagTokens .= $ch;
                                $ch = $tmpScanner->getChar();
                                if (isWhitespace($ch)){
                                    $intSkipped = $tmpScanner->skipWhiteSpaces();
                                    $strTagTokens .= str_repeat(' ', ++$intSkipped);
                                    $ch = $tmpScanner->getChar();
                                    if ($ch==DTS_VARIABLE_STICK){
                                        $strVariableName .= $ch;
                                        $strTagTokens .= $ch;
                                        $intSkipped = $tmpScanner->skipWhiteSpaces();
                                        $strTagTokens .= str_repeat(' ', $intSkipped);
                                        $ch = $tmpScanner->getChar();
                                    }
                                    else
                                        $boolCanOut = TRUE;
                                }
                                if($ch===NULL) break;
                            }while(($ch!=DTS_TAG_END)&&($ch!=DTS_MODIFIER)&&(!$boolCanOut));
                            $strVariableName = ltrim($strVariableName, DTS_VARIABLE);
                            if (isValidVariable($strVariableName)){
                                $strVariableName = stringCleanChars($strVariableName, DTS_WHITESPACE,'');
                                $tToken->setProperty('variable', $strVariableName);
                                if (ctype_digit($ch)||($ch==DTS_NUM_SIGN_MINUS)||($ch==DTS_NUM_SIGN_PLUS)){
                                    $strNumber = '';
                                    do{
                                        $strNumber .= $ch;
                                        $strTagTokens .= $ch;
                                        $ch = $tmpScanner->getChar();
                                        if ($ch===NULL) break;
                                    }while(($ch!=DTS_TAG_END)&&($ch!=DTS_MODIFIER));
                                    if($ch!==NULL){
                                        $strNumber = (double)$strNumber;
                                        $tToken->setAction(DTA_ASSIGN);
                                        $tToken->setProperty('number', $strNumber);
                                        $tToken->setProperty('type', 'number');
                                    }
                                    else{
                                        while(($ch!=DTS_TAG_END)&&($ch!==NULL)){
                                            $strTagTokens .= $ch;
                                            $ch = $tmpScanner->getChar();
                                        }
                                        $strTagTokens .= $ch;
                                        $tToken = new Token(DTT_ERROR, DTA_PRINT_OUT);
                                        $tToken->setProperty('error', 'Variable Statement is incomplete.');
                                        $tToken->setProperty('tag', $strTagTokens);
                                    }
                                }
                                elseif($ch==DTS_VARIABLE){
                                    $strVariableName = '';
                                    do{
                                        $strVariableName .= $ch;
                                        $strTagTokens .= $ch;
                                        $ch = $tmpScanner->getChar();
                                        if ($ch===NULL) break;
                                    }while(($ch!=DTS_TAG_END)&&($ch!=DTS_MODIFIER));
                                    if($ch!==NULL){
                                        $strVariableName = stringCleanChars($strVariableName, DTS_WHITESPACE,'');
                                        $strVariableName = ltrim($strVariableName, DTS_VARIABLE);
                                        if (isValidVariable($strVariableName)){
                                            $tToken->setAction(DTA_ASSIGN);
                                            $tToken->setProperty('fromvar', $strVariableName);
                                            $tToken->setProperty('type', 'fromvar');
                                        }
                                        else{
                                            while(($ch!=DTS_TAG_END)&&($ch!==NULL)){
                                                $strTagTokens .= $ch;
                                                $ch = $tmpScanner->getChar();
                                            }
                                            $strTagTokens .= $ch;
                                            $tToken = new Token(DTT_ERROR, DTA_PRINT_OUT);
                                            $tToken->setProperty('error', 'Variable Statement : variable property is invalid.');
                                            $tToken->setProperty('tag', $strTagTokens);
                                        }
                                    }
                                    else{
                                        while(($ch!=DTS_TAG_END)&&($ch!==NULL)){
                                            $strTagTokens .= $ch;
                                            $ch = $tmpScanner->getChar();
                                        }
                                        $strTagTokens .= $ch;
                                        $tToken = new Token(DTT_ERROR, DTA_PRINT_OUT);
                                        $tToken->setProperty('error', 'Variable Statement is incomplete.');
                                        $tToken->setProperty('tag', $strTagTokens);
                                    }
                                }
                                elseif(($ch==DTS_STRING_1)||($ch==DTS_STRING_2)){
                                    $strSign = $ch;
                                    $strString = '';
                                    do{
                                        $strString .= $ch;
                                        $strTagTokens .= $ch;
                                        $ch = $tmpScanner->getChar();
                                        if ($ch===NULL) break;
                                    }while($ch!=$strSign);
                                    if ($ch!==NULL){
                                        $strString .= $ch;
                                        $strString = stringTrimOnce2a($strString, $strSign);
                                        $tToken->setAction(DTA_ASSIGN);
                                        $tToken->setProperty('string', $strString);
                                        $tToken->setProperty('type', 'string');
                                        $intSkipped = $tmpScanner->skipWhiteSpaces();
                                        $strTagTokens .= str_repeat(" ", $intSkipped);
                                        $ch = $tmpScanner->getChar();
                                    }
                                    else{
                                        while(($ch!=DTS_TAG_END)&&($ch!==NULL)){
                                            $strTagTokens .= $ch;
                                            $ch = $tmpScanner->getChar();
                                        }
                                        $strTagTokens .= $ch;
                                        $tToken = new Token(DTT_ERROR, DTA_PRINT_OUT);
                                        $tToken->setProperty('error', 'Variable Statement : string property is incomplete.');
                                        $tToken->setProperty('tag', $strTagTokens);
                                    }
                                }
                                elseif ($ch==DTS_EXPRESSION_START){
                                    $strExpression = '';
                                    $boolCanOut = FALSE;
                                    do{
                                        $strExpression .= $ch;
                                        $strTagTokens .= $ch;
                                        $ch = $tmpScanner->getChar();
                                        if ($ch===NULL) break;
                                        if($ch==DTS_EXPRESSION_END){
                                            $strExpression .= $ch;
                                            $strTagTokens .= $ch;
                                            $intSkipped = $tmpScanner->skipWhiteSpaces();
                                            $strTagTokens .= str_repeat(' ', $intSkipped);
                                            $ch = $tmpScanner->getChar();
                                            if (($ch==DTS_TAG_END)||($ch==DTS_MODIFIER))
                                                $boolCanOut = TRUE;
                                        }
                                    }while(!$boolCanOut);
                                    if ($ch!==NULL){
                                        $strExpression = stringTrimOnce3a($strExpression, DTS_EXPRESSION_START, DTS_EXPRESSION_END);
                                        if (isValidExpression($strExpression)){
                                            $tToken->setAction(DTA_ASSIGN);
                                            $tToken->setProperty('expression', $strExpression);
                                            $tToken->setProperty('type', 'expression');
                                            $intSkipped = $tmpScanner->skipWhiteSpaces();
                                            $strTagTokens .= str_repeat(" ", $intSkipped);
                                        }
                                        else{
                                            while(($ch!=DTS_TAG_END)&&($ch!==NULL)){
                                                $strTagTokens .= $ch;
                                                $ch = $tmpScanner->getChar();
                                            }
                                            $strTagTokens .= $ch;
                                            $tToken = new Token(DTT_ERROR, DTA_PRINT_OUT);
                                            $tToken->setProperty('error', 'Variable Statement : expression property is invalid.');
                                            $tToken->setProperty('tag', $strTagTokens);
                                        }
                                    }
                                    else{
                                        while(($ch!=DTS_TAG_END)&&($ch!==NULL)){
                                            $strTagTokens .= $ch;
                                            $ch = $tmpScanner->getChar();
                                        }
                                        $strTagTokens .= $ch;
                                        $tToken = new Token(DTT_ERROR, DTA_PRINT_OUT);
                                        $tToken->setProperty('error', 'Variable Statement : expression property is incomplete.');
                                        $tToken->setProperty('tag', $strTagTokens);
                                    }
                                }
                                if ($ch==DTS_MODIFIER){
                                    $strModifier = '';
                                    $boolCanOut = false;
                                    do{
                                        $strModifier .= $ch;
                                        $strTagTokens .= $ch;
                                        $ch = $tmpScanner->getChar();
                                        if ($ch===NULL) break;
                                        if (($ch==DTS_STRING_1)||($ch==DTS_STRING_2))
                                            $boolCanOut = !$boolCanOut;
                                    }while(($boolCanOut)||($ch!=DTS_TAG_END));
                                    if($ch!==NULL){
                                        $strModifier = ltrim($strModifier, DTS_MODIFIER);
                                        $tToken->setProperty('modifier', $strModifier);
                                    }
                                    else{
                                        while(($ch!=DTS_TAG_END)&&($ch!==NULL)){
                                            $strTagTokens .= $ch;
                                            $ch = $tmpScanner->getChar();
                                        }
                                        $strTagTokens .= $ch;
                                        $tToken = new Token(DTT_ERROR, DTA_PRINT_OUT);
                                        $tToken->setProperty('error', 'Variable Statement is incomplete.');
                                        $tToken->setProperty('tag', $strTagTokens);
                                    }
                                }
                            }
                            elseif($ch===NULL){
                                $tToken = new Token(DTT_ERROR, DTA_PRINT_OUT);
                                $tToken->setProperty('error', 'Variable Statement is incomplete.');
                                $tToken->setProperty('tag', $strTagTokens);
                            }
                            else{
                                $strTagTokens .= $ch;
                                $tToken = new Token(DTT_ERROR, DTA_PRINT_OUT);
                                $tToken->setProperty('error', 'Variable name is incorrect.');
                                $tToken->setProperty('tag', $strTagTokens);
                            }
                            break;
                        case DTS_EXPRESSION_START:
                            $tToken = new Token(DTT_EXPRESSION, DTA_PRINT_OUT);
                            $strExpression = '';
                            $boolCanOut = FALSE;
                            do{
                                $strExpression .= $ch;
                                $strTagTokens .= $ch;
                                $ch = $tmpScanner->getChar();
                                if ($ch===NULL) break;
                                if($ch==DTS_EXPRESSION_END){
                                    $strExpression .= $ch;
                                    $strTagTokens .= $ch;
                                    $intSkipped = $tmpScanner->skipWhiteSpaces();
                                    $strTagTokens .= str_repeat(' ', $intSkipped);
                                    $ch = $tmpScanner->getChar();
                                    if (($ch==DTS_TAG_END)||($ch==DTS_MODIFIER))
                                        $boolCanOut = TRUE;
                                }
                            }while(!$boolCanOut);
                            if ($ch!==NULL){
                                $strExpression = stringTrimOnce3a($strExpression, DTS_EXPRESSION_START, DTS_EXPRESSION_END);
                                if (isValidExpression($strExpression)){
                                    $tToken->setProperty('expression', $strExpression);
                                    $intSkipped = $tmpScanner->skipWhiteSpaces();
                                    $strTagTokens .= str_repeat(" ", $intSkipped);
                                    if ($ch==DTS_MODIFIER){
                                        $strModifier = '';
                                        $boolCanOut = false;
                                        do{
                                            $strModifier .= $ch;
                                            $strTagTokens .= $ch;
                                            $ch = $tmpScanner->getChar();
                                            if ($ch===NULL) break;
                                            if (($ch==DTS_STRING_1)||($ch==DTS_STRING_2))
                                                $boolCanOut = !$boolCanOut;
                                        }while(($boolCanOut)||($ch!=DTS_TAG_END));
                                        if($ch!==NULL){
                                            $strModifier = ltrim($strModifier, DTS_MODIFIER);
                                            $tToken->setProperty('modifier', $strModifier);
                                        }
                                        else{
                                            while(($ch!=DTS_TAG_END)&&($ch!==NULL)){
                                                $strTagTokens .= $ch;
                                                $ch = $tmpScanner->getChar();
                                            }
                                            $strTagTokens .= $ch;
                                            $tToken = new Token(DTT_ERROR, DTA_PRINT_OUT);
                                            $tToken->setProperty('error', 'Variable Statement is incomplete.');
                                            $tToken->setProperty('tag', $strTagTokens);
                                        }
                                    }
                                }
                                else{
                                    while(($ch!=DTS_TAG_END)&&($ch!==NULL)){
                                        $strTagTokens .= $ch;
                                        $ch = $tmpScanner->getChar();
                                    }
                                    $strTagTokens .= $ch;
                                    $tToken = new Token(DTT_ERROR, DTA_PRINT_OUT);
                                    $tToken->setProperty('error', 'Expression Statement : expression is invalid.');
                                    $tToken->setProperty('tag', $strTagTokens);
                                }
                            }
                            else{
                                while(($ch!=DTS_TAG_END)&&($ch!==NULL)){
                                    $strTagTokens .= $ch;
                                    $ch = $tmpScanner->getChar();
                                }
                                $strTagTokens .= $ch;
                                $tToken = new Token(DTT_ERROR, DTA_PRINT_OUT);
                                $tToken->setProperty('error', 'Expression Statement : expression is incomplete.');
                                $tToken->setProperty('tag', $strTagTokens);
                            }
                            break;
                        case DTS_STMT_END:
                            $tToken = new Token(DTT_HTML_TAG, DTA_DO_NOTHING);
                            $strTagName = '';
                            do{
                                $strTagName .= $ch;
                                $strTagTokens .= $ch;
                                $ch = $tmpScanner->getChar();
                                if ($ch===NULL) break;
                            }while($ch!=DTS_TAG_END);
                            $strTagName = ltrim($strTagName, DTS_STMT_END);
                            $strTagName = strtoupper($strTagName);
                            switch($strTagName){
                                case DTS_STMT_IF:
                                    $tToken->setType(DTT_STMT_END_IF);
                                    break;
                                case DTS_STMT_LOOP:
                                    $tToken->setType(DTT_STMT_END_LOOP);
                                    $tToken->setAction(DTA_CONTINUE_LOOP);
                                    break;
                                case DTS_STMT_FOR:
                                    $tToken->setType(DTT_STMT_END_FOR);
                                    $tToken->setAction(DTA_CONTINUE_LOOP);
                                    break;
                                default:
                                    while(($ch!=DTS_TAG_END)&&($ch!==NULL)){
                                        $strTagTokens .= $ch;
                                        $ch = $tmpScanner->getChar();
                                    }
                                    if ($ch==DTS_TAG_END)
                                        $strTagTokens .= $ch;
                                    if (isValidStatement($strTagName)){
                                        $tToken->setAction(DTA_PRINT_OUT);
                                        $tToken->setProperty('tag', $strTagTokens);
                                    }
                                    else{
                                        $tToken = new Token(DTT_ERROR, DTA_PRINT_OUT);
                                        $tToken->setProperty('error', 'The tag is wrong.');
                                        $tToken->setProperty('tag', $strTagTokens);
                                    }
                            }
                            break;
                        case DTS_COMMENT:
                            $tToken = new Token(DTT_COMMENT, DTA_DO_NOTHING);
                            $strComment = '';
                            $boolCanOut = FALSE;
                            do{
                                $strComment .= $ch;
                                $strTagTokens .= $ch;
                                $ch = $tmpScanner->getChar();
                                if ($ch===NULL) break;
                                if($ch==DTS_COMMENT){
                                    $strComment .= $ch;
                                    $strTagTokens .= $ch;
                                    $intSkipped = $tmpScanner->skipWhiteSpaces();
                                    $strTagTokens .= str_repeat(' ', $intSkipped);
                                    $ch = $tmpScanner->getChar();
                                    if ($ch==DTS_TAG_END)
                                        $boolCanOut = TRUE;
                                }
                            }while(!$boolCanOut);
                            if ($ch!==NULL){
                                $strComment = stringTrimOnce2a($strComment, DTS_COMMENT);
                                $tToken->setProperty('comment', $strComment);
                            }
                            else{
                                while(($ch!=DTS_TAG_END)&&($ch!==NULL)){
                                    $strTagTokens .= $ch;
                                    $ch = $tmpScanner->getChar();
                                }
                                $strTagTokens .= $ch;
                                $tToken = new Token(DTT_HTML_TAG, DTA_PRINT_OUT);
                                $tToken->setProperty('tag', $strTagTokens);
                            }
                            break;
                        default:
                            do{
                                $strTagTokens .= $ch;
                                $ch = $tmpScanner->getChar();
                                if($ch===NULL) break;
                            }while($ch!=DTS_TAG_START);
                            $tToken->setProperty('tag', $strTagTokens);
                            if ($ch!==NULL)
                                $tmpScanner->goBackAChar();
                    }
                }
                elseif (ctype_alpha($ch)){
                    $strTagName = '';
                    do{
                        $strTagName .= $ch;
                        $strTagTokens .= $ch;
                        $ch = $tmpScanner->getChar();
                        if ($ch===NULL) break;
                    }while(!isWhitespace($ch)&&($ch!=DTS_TAG_END)&&($ch!=DTS_TAG_START));
                    if ($ch!==NULL){
                        if(($ch!=DTS_TAG_END) && ($ch!=DTS_TAG_START)){
                            $intSkipped = $tmpScanner->skipWhiteSpaces();
                            if (isWhitespace($ch)) $intSkipped++;
                            $strTagTokens .= str_repeat(" ", $intSkipped);
                            $ch = $tmpScanner->getChar();
                        }
                        $strTagName = strtoupper($strTagName);
                        switch($strTagName){
                            case DTS_STMT_IF:
                                $tToken = new Token(DTT_STMT_IF, DTA_STMT_DO);
                                $strExpression = '';
                                $boolCanOut = FALSE;
                                do{
                                    if ($ch!=NULL){
                                        $strExpression .= $ch;
                                        $strTagTokens .= $ch;
                                    }
                                    $ch = $tmpScanner->getChar();
                                    if ($ch===NULL) break;
                                    if($ch==DTS_EXPRESSION_END){
                                        $strExpression .= $ch;
                                        $strTagTokens .= $ch;
                                        $intSkipped = $tmpScanner->skipWhiteSpaces();
                                        $strTagTokens .= str_repeat(' ', $intSkipped);
                                        $ch = $tmpScanner->getChar();
                                        if ($ch==DTS_TAG_END)
                                            $boolCanOut = TRUE;
                                        elseif ($ch==DTS_EXPRESSION_END){
                                            $ch = NULL;
                                            $tmpScanner->goBackAChar();
                                        }
                                    }
                                }while(!$boolCanOut);
                                if ($ch!==NULL){
                                    $strExpression = stringTrimOnce3a($strExpression, DTS_EXPRESSION_START, DTS_EXPRESSION_END);
                                    if (isValidExpression($strExpression)){
                                        $tToken->setProperty('expression', $strExpression);
                                    }
                                    else{
                                        while(($ch!=DTS_TAG_END)&&($ch!==NULL)){
                                            $strTagTokens .= $ch;
                                            $ch = $tmpScanner->getChar();
                                        }
                                        $strTagTokens .= $ch;
                                        $tToken = new Token(DTT_ERROR, DTA_PRINT_OUT);
                                        $tToken->setProperty('error', 'IF Statement : expression property is invalid.');
                                        $tToken->setProperty('tag', $strTagTokens);
                                    }
                                }
                                else{
                                    while(($ch!=DTS_TAG_END)&&($ch!==NULL)){
                                        $strTagTokens .= $ch;
                                        $ch = $tmpScanner->getChar();
                                    }
                                    $strTagTokens .= $ch;
                                    $tToken = new Token(DTT_ERROR, DTA_PRINT_OUT);
                                    $tToken->setProperty('error', 'IF Statement is incomplete.');
                                    $tToken->setProperty('tag', $strTagTokens);
                                }
                                break;
                            case DTS_STMT_ELSE:
                                if ($ch==DTS_TAG_END){
                                    $tToken = new Token(DTT_STMT_ELSE, DTA_DO_NOTHING);
                                }
                                else{
                                    $tToken = new Token(DTT_STMT_ELSE_IF, DTA_STMT_DO);
                                    $strExpression = '';
                                    $boolCanOut = FALSE;
                                    do{
                                        if ($ch!=NULL){
                                            $strExpression .= $ch;
                                            $strTagTokens .= $ch;
                                        }
                                        $ch = $tmpScanner->getChar();
                                        if ($ch===NULL) break;
                                        if($ch==DTS_EXPRESSION_END){
                                            $strExpression .= $ch;
                                            $strTagTokens .= $ch;
                                            $intSkipped = $tmpScanner->skipWhiteSpaces();
                                            $strTagTokens .= str_repeat(' ', $intSkipped);
                                            $ch = $tmpScanner->getChar();
                                            if ($ch==DTS_TAG_END)
                                                $boolCanOut = TRUE;
                                            elseif ($ch==DTS_EXPRESSION_END){
                                                $ch = NULL;
                                                $tmpScanner->goBackAChar();
                                            }
                                        }
                                    }while(!$boolCanOut);
                                    if ($ch!==NULL){
                                        $strExpression = stringTrimOnce3a($strExpression, DTS_EXPRESSION_START, DTS_EXPRESSION_END);
                                        if (isValidExpression($strExpression)){
                                            $tToken->setProperty('expression', $strExpression);
                                        }
                                        else{
                                            while(($ch!=DTS_TAG_END)&&($ch!==NULL)){
                                                $strTagTokens .= $ch;
                                                $ch = $tmpScanner->getChar();
                                            }
                                            $strTagTokens .= $ch;
                                            $tToken = new Token(DTT_ERROR, DTA_PRINT_OUT);
                                            $tToken->setProperty('error', 'IF Statement : ELSE\'s expression property is invalid.');
                                            $tToken->setProperty('tag', $strTagTokens);
                                        }
                                    }
                                    else{
                                        while(($ch!=DTS_TAG_END)&&($ch!==NULL)){
                                            $strTagTokens .= $ch;
                                            $ch = $tmpScanner->getChar();
                                        }
                                        $strTagTokens .= $ch;
                                        $tToken = new Token(DTT_ERROR, DTA_PRINT_OUT);
                                        $tToken->setProperty('error', 'IF Statement is incomplete.');
                                        $tToken->setProperty('tag', $strTagTokens);
                                    }
                                }
                                break;
                            case DTS_STMT_FOR:
                                $tToken = new Token(DTT_STMT_FOR, DTA_STMT_DO);
                                if ($ch==DTS_VARIABLE){
                                    $strVariableName = '';
                                    $boolCanOut = FALSE;
                                    do{
                                        $strVariableName .= $ch;
                                        $strTagTokens .= $ch;
                                        $ch = $tmpScanner->getChar();
                                        if (isWhitespace($ch)){
                                            $intSkipped = $tmpScanner->skipWhiteSpaces();
                                            $strTagTokens .= str_repeat(' ', ++$intSkipped);
                                            $ch = $tmpScanner->getChar();
                                            if ($ch==DTS_VARIABLE_STICK){
                                                $strVariableName .= $ch;
                                                $strTagTokens .= $ch;
                                                $intSkipped = $tmpScanner->skipWhiteSpaces();
                                                $strTagTokens .= str_repeat(' ', $intSkipped);
                                                $ch = $tmpScanner->getChar();
                                            }
                                            else
                                                $boolCanOut = TRUE;
                                        }
                                        if($ch===NULL) break;
                                    }while(($ch!=DTS_TAG_END)&&($ch!=DTS_VARIABLE)&&(!$boolCanOut));
                                    if($ch!==NULL){
                                        $strVariableName = stringCleanChars($strVariableName, DTS_WHITESPACE,'');
                                        $strVariableName = ltrim($strVariableName, DTS_VARIABLE);
                                        if (isValidVariable($strVariableName)){
                                            $tToken->setProperty('var_count', $strVariableName);
                                            $tToken->setProperty('type', 'var_count');
                                        }
                                        else{
                                            while(($ch!=DTS_TAG_END)&&($ch!==NULL)){
                                                $strTagTokens .= $ch;
                                                $ch = $tmpScanner->getChar();
                                            }
                                            $strTagTokens .= $ch;
                                            $tToken = new Token(DTT_ERROR, DTA_PRINT_OUT);
                                            $tToken->setProperty('error', 'FOR Statement : first property is invalid variable.');
                                            $tToken->setProperty('tag', $strTagTokens);
                                        }
                                    }
                                    else{
                                        while(($ch!=DTS_TAG_END)&&($ch!==NULL)){
                                            $strTagTokens .= $ch;
                                            $ch = $tmpScanner->getChar();
                                        }
                                        $strTagTokens .= $ch;
                                        $tToken = new Token(DTT_ERROR, DTA_PRINT_OUT);
                                        $tToken->setProperty('error', 'FOR Statement is incomplete.');
                                        $tToken->setProperty('tag', $strTagTokens);
                                    }
                                }
                                elseif ($ch==DTS_EXPRESSION_START){
                                    $strExpression = '';
                                    $boolCanOut = FALSE;
                                    do{
                                        if ($ch!=NULL){
                                            $strExpression .= $ch;
                                            $strTagTokens .= $ch;
                                        }
                                        $ch = $tmpScanner->getChar();
                                        if ($ch===NULL) break;
                                        if($ch==DTS_EXPRESSION_END){
                                            $strExpression .= $ch;
                                            $strTagTokens .= $ch;
                                            $intSkipped = $tmpScanner->skipWhiteSpaces();
                                            $strTagTokens .= str_repeat(' ', $intSkipped);
                                            $ch = $tmpScanner->getChar();
                                            if (($ch==DTS_TAG_END) || ($ch==DTS_VARIABLE))
                                                $boolCanOut = TRUE;
                                            elseif ($ch==DTS_EXPRESSION_END){
                                                $ch = NULL;
                                                $tmpScanner->goBackAChar();
                                            }
                                        }
                                    }while(!$boolCanOut);
                                    if ($ch!==NULL){
                                        $strExpression = stringTrimOnce3a($strExpression, DTS_EXPRESSION_START, DTS_EXPRESSION_END);
                                        if (isValidExpression($strExpression)){
                                            $tToken->setProperty('exp_count', $strExpression);
                                            $tToken->setProperty('type', 'exp_count');
                                        }
                                        else{
                                            while(($ch!=DTS_TAG_END)&&($ch!==NULL)){
                                                $strTagTokens .= $ch;
                                                $ch = $tmpScanner->getChar();
                                            }
                                            $strTagTokens .= $ch;
                                            $tToken = new Token(DTT_ERROR, DTA_PRINT_OUT);
                                            $tToken->setProperty('error', 'FOR Statement : first property is invalid expression.');
                                            $tToken->setProperty('tag', $strTagTokens);
                                        }
                                    }
                                    else{
                                        while(($ch!=DTS_TAG_END)&&($ch!==NULL)){
                                            $strTagTokens .= $ch;
                                            $ch = $tmpScanner->getChar();
                                        }
                                        $strTagTokens .= $ch;
                                        $tToken = new Token(DTT_ERROR, DTA_PRINT_OUT);
                                        $tToken->setProperty('error', 'FOR Statement : expression property is incomplete.');
                                        $tToken->setProperty('tag', $strTagTokens);
                                    }
                                }
                                elseif ((ctype_digit($ch))||($ch==DTS_NUM_SIGN_PLUS)||($ch==DTS_NUM_SIGN_MINUS)){
                                    $strNumber = '';
                                    do{
                                        $strNumber .= $ch;
                                        $strTagTokens .= $ch;
                                        $ch = $tmpScanner->getChar();
                                        if ($ch===NULL) break;
                                    }while(($ch!=DTS_TAG_END)&&($ch!=DTS_VARIABLE));
                                    if($ch!==NULL){
                                        $strNumber = round(abs((double)$strNumber));
                                        $tToken->setProperty('num_count', $strNumber);
                                        $tToken->setProperty('type', 'num_count');
                                    }
                                    else{
                                        while(($ch!=DTS_TAG_END)&&($ch!==NULL)){
                                            $strTagTokens .= $ch;
                                            $ch = $tmpScanner->getChar();
                                        }
                                        $strTagTokens .= $ch;
                                        $tToken = new Token(DTT_ERROR, DTA_PRINT_OUT);
                                        $tToken->setProperty('error', 'FOR Statement is incomplete.');
                                        $tToken->setProperty('tag', $strTagTokens);
                                    }
                                }
                                else{
                                    while(($ch!=DTS_TAG_END)&&($ch!==NULL)){
                                        $strTagTokens .= $ch;
                                        $ch = $tmpScanner->getChar();
                                    }
                                    $strTagTokens .= $ch;
                                    $tToken = new Token(DTT_ERROR, DTA_PRINT_OUT);
                                    $tToken->setProperty('error', 'FOR Statement : first property is invalid.');
                                    $tToken->setProperty('tag', $strTagTokens);
                                }
                                if ($ch==DTS_VARIABLE){
                                    $strVariableName = '';
                                    do{
                                        $strVariableName .= $ch;
                                        $strTagTokens .= $ch;
                                        $ch = $tmpScanner->getChar();
                                        if ($ch===NULL) break;
                                    }while($ch!=DTS_TAG_END);
                                    if($ch!==NULL){
                                        $strVariableName = stringCleanChars($strVariableName, DTS_WHITESPACE,'');
                                        $strVariableName = ltrim($strVariableName, DTS_VARIABLE);
                                        if (isValidVariable($strVariableName)){
                                            $tToken->setProperty('variable', $strVariableName);
                                        }
                                        else{
                                            while(($ch!=DTS_TAG_END)&&($ch!==NULL)){
                                                $strTagTokens .= $ch;
                                                $ch = $tmpScanner->getChar();
                                            }
                                            $strTagTokens .= $ch;
                                            $tToken = new Token(DTT_ERROR, DTA_PRINT_OUT);
                                            $tToken->setProperty('error', 'FOR Statement : second property is invalid variable.');
                                            $tToken->setProperty('tag', $strTagTokens);
                                        }
                                    }
                                    else{
                                        while(($ch!=DTS_TAG_END)&&($ch!==NULL)){
                                            $strTagTokens .= $ch;
                                            $ch = $tmpScanner->getChar();
                                        }
                                        $strTagTokens .= $ch;
                                        $tToken = new Token(DTT_ERROR, DTA_PRINT_OUT);
                                        $tToken->setProperty('error', 'FOR Statement is incomplete.');
                                        $tToken->setProperty('tag', $strTagTokens);
                                    }
                                }
                                break;
                            case DTS_STMT_LOOP:
                                $tToken = new Token(DTT_STMT_LOOP, DTA_STMT_DO);
                                if ($ch==DTS_VARIABLE){
                                    $strVariableName = '';
                                    do{
                                        $strVariableName .= $ch;
                                        $strTagTokens .= $ch;
                                        $ch = $tmpScanner->getChar();
                                        if ($ch===NULL) break;
                                    }while($ch!=DTS_TAG_END);
                                    if($ch!==NULL){
                                        $strVariableName = stringCleanChars($strVariableName, DTS_WHITESPACE,'');
                                        $strVariableName = ltrim($strVariableName, DTS_VARIABLE);
                                        if (isValidVariable($strVariableName)){
                                            $tToken->setProperty('table', $strVariableName);
                                        }
                                        else{
                                            while(($ch!=DTS_TAG_END)&&($ch!==NULL)){
                                                $strTagTokens .= $ch;
                                                $ch = $tmpScanner->getChar();
                                            }
                                            $strTagTokens .= $ch;
                                            $tToken = new Token(DTT_ERROR, DTA_PRINT_OUT);
                                            $tToken->setProperty('error', 'LOOP Statement : the property is invalid variable.');
                                            $tToken->setProperty('tag', $strTagTokens);
                                        }
                                    }
                                    else{
                                        while(($ch!=DTS_TAG_END)&&($ch!==NULL)){
                                            $strTagTokens .= $ch;
                                            $ch = $tmpScanner->getChar();
                                        }
                                        $strTagTokens .= $ch;
                                        $tToken = new Token(DTT_ERROR, DTA_PRINT_OUT);
                                        $tToken->setProperty('error', 'LOOP Statement is incomplete.');
                                        $tToken->setProperty('tag', $strTagTokens);
                                    }
                                }
                                else{
                                    while(($ch!=DTS_TAG_END)&&($ch!==NULL)){
                                        $strTagTokens .= $ch;
                                        $ch = $tmpScanner->getChar();
                                    }
                                    $strTagTokens .= $ch;
                                    $tToken = new Token(DTT_ERROR, DTA_PRINT_OUT);
                                    $tToken->setProperty('error', 'LOOP Statement needs a variable.');
                                    $tToken->setProperty('tag', $strTagTokens);
                                }
                                break;
                            case DTS_STMT_LOAD:
                                $tToken = new Token(DTT_STMT_LOAD, DTA_STMT_DO);
                                if(($ch==DTS_STRING_1)||($ch==DTS_STRING_2)){
                                    $strSign = $ch;
                                    $strString = '';
                                    do{
                                        $strString .= $ch;
                                        $strTagTokens .= $ch;
                                        $ch = $tmpScanner->getChar();
                                        if ($ch===NULL) break;
                                    }while($ch!=$strSign);
                                    if ($ch!==NULL){
                                        $strString .= $ch;
                                        $strString = stringTrimOnce2a($strString, $strSign);
                                        $tToken->setProperty('filename', $strString);
                                        $tmpScanner->skipWhiteSpaces();
                                        $tmpScanner->getChar();
                                    }
                                    else{
                                        while(($ch!=DTS_TAG_END)&&($ch!==NULL)){
                                            $strTagTokens .= $ch;
                                            $ch = $tmpScanner->getChar();
                                        }
                                        $strTagTokens .= $ch;
                                        $tToken = new Token(DTT_ERROR, DTA_PRINT_OUT);
                                        $tToken->setProperty('error', 'LOAD Statement : file name property is incomplete.');
                                        $tToken->setProperty('tag', $strTagTokens);
                                    }
                                }
                                else{
                                    while(($ch!=DTS_TAG_END)&&($ch!==NULL)){
                                        $strTagTokens .= $ch;
                                        $ch = $tmpScanner->getChar();
                                    }
                                    $strTagTokens .= $ch;
                                    $tToken = new Token(DTT_ERROR, DTA_PRINT_OUT);
                                    $tToken->setProperty('error', 'LOAD Statement needs a filename.');
                                    $tToken->setProperty('tag', $strTagTokens);
                                }
                                break;
                            default:
                                while((($ch!=DTS_TAG_END) && ($ch!=DTS_TAG_START))&&($ch!==NULL)){
                                    $strTagTokens .= $ch;
                                    $ch = $tmpScanner->getChar();
                                }
                                if ($ch==DTS_TAG_END)
                                    $strTagTokens .= $ch;
                                if (isValidStatement($strTagName)){
                                    $tToken = new Token(DTT_HTML_TAG, DTA_PRINT_OUT);
                                    $tToken->setProperty('tag', $strTagTokens);
                                }
                                else{
                                    $tToken = new Token(DTT_ERROR, DTA_PRINT_OUT);
                                    $tToken->setProperty('error', 'The tag is wrong.');
                                    $tToken->setProperty('tag', $strTagTokens);
                                }
                                if ($ch==DTS_TAG_START){
                                    $tmpScanner->goBackAChar();
                                    continue;
                                }
                        }
                    }
                    else{
                        $strTagTokens .= $ch;
                        $tToken = new Token(DTT_ERROR, DTA_PRINT_OUT);
                        $tToken->setProperty('error', 'This tag is incomplete.');
                        $tToken->setProperty('tag', $strTagTokens);
                    }
                }
                else{
                    do{
                        $strTagTokens .= $ch;
                        $ch = $tmpScanner->getChar();
                        if($ch===NULL) break;
                    }while($ch!=DTS_TAG_START);
                    $tToken->setProperty('tag', $strTagTokens);
                    $tToken->setAction(DTA_DO_NOTHING);
                    if ($ch!==NULL)
                        $tmpScanner->goBackAChar();
                }
            }
            elseif($ch!==NULL){
                do{
                    $ch = $tmpScanner->getChar();
                    if($ch===NULL) break;
                    $strTagTokens .= $ch;
                }while($ch!=DTS_TAG_START);
                $strTagTokens = rtrim($strTagTokens, DTS_TAG_START);
                $tToken->setProperty('tag', $strTagTokens);
                if ($ch!==NULL)
                    $tmpScanner->goBackAChar();
            }
            $this->arrTokens[] = $tToken;
        }while(!$tmpScanner->endOfFile());
        $this->tagsNormalizer();
        $this->statementNormalizer();
    }
    private function statementNormalizer(){
        $loopStack = new Stack();
        $intIndex = -1;
        foreach($this->arrTokens as &$tToken){
            $intIndex++;
            if (($tToken->getType()==DTT_STMT_FOR)||($tToken->getType()==DTT_STMT_LOOP)||($tToken->getType()==DTT_STMT_IF)){
                $arrOptionToken = array('token'=>$tToken, 'pos'=>$intIndex);
                $loopStack->pushItem($arrOptionToken);
            }
            elseif($tToken->getType()==DTT_STMT_END_FOR){
                $canContinue = TRUE;
                do{
                    $tmpToken = $loopStack->popItem();
                    if ($tmpToken!==NULL){
                        if ($tmpToken['token']->getType()==DTT_STMT_FOR){
                            $tmpToken['token']->setProperty('endfor', $intIndex);
                            $tToken->setProperty('goto', $tmpToken['pos']);
                            $canContinue = FALSE;
                        }
                        else{
                            $strError = $this->getMessageType($tmpToken['token']->getType());
                            $tmpToken['token']->setType(DTT_ERROR);
                            $tmpToken['token']->setAction(DTA_PRINT_OUT);
                            $tmpToken['token']->setProperties();
                            $tmpToken['token']->setProperty('error', $strError);
                        }
                    }
                    else{
                        $tToken->setType(DTT_ERROR);
                        $tToken->setAction(DTA_PRINT_OUT);
                        $tToken->setExtraDataArray();
                        $tToken->setExtraData('error', 'FOR statement : stripped END_FOR </for>');
                        $canContinue = FALSE;
                    }
                }while($canContinue);
            }
            elseif($tToken->getType()==DTT_STMT_END_LOOP){
                $canContinue = TRUE;
                do{
                    $tmpToken = $loopStack->popItem();
                    if ($tmpToken!==NULL){
                        if ($tmpToken['token']->getType()==DTT_STMT_LOOP){
                            $tmpToken['token']->setProperty('endloop', $intIndex);
                            $tToken->setProperty('goto', $tmpToken['pos']);
                            $canContinue = FALSE;
                        }
                        else{
                            $strError = $this->getMessageType($tmpToken['token']->getType());
                            $tmpToken['token']->setType(DTT_ERROR);
                            $tmpToken['token']->setAction(DTA_PRINT_OUT);
                            $tmpToken['token']->setProperties();
                            $tmpToken['token']->setProperty('error', $strError);
                        }
                    }
                    else{
                        $tToken->setType(DTT_ERROR);
                        $tToken->setAction(DTA_PRINT_OUT);
                        $tToken->setExtraDataArray();
                        $tToken->setExtraData('error', 'LOOP statement : stripped END_LOOP </loop>');
                        $canContinue = FALSE;
                    }
                }while($canContinue);
            }
            elseif(($tToken->getType()==DTT_STMT_ELSE_IF)||($tToken->getType()==DTT_STMT_ELSE)){
                $canContinue = TRUE;
                do{
                    $tmpToken = $loopStack->popItem();
                    if ($tmpToken!==NULL){
                        if (($tmpToken['token']->getType()==DTT_STMT_IF)||($tmpToken['token']->getType()==DTT_STMT_ELSE_IF)){
                            $tmpToken['token']->setProperty('else', $intIndex);
                            $loopStack->pushItem($tmpToken);
                            $arrOptionToken = array('token'=>$tToken, 'pos'=>$intIndex);
                            $loopStack->pushItem($arrOptionToken);
                            $canContinue = FALSE;
                        }
                        else{
                            $strError = $this->getMessageType($tmpToken['token']->getType());
                            $tmpToken['token']->setType(DTT_ERROR);
                            $tmpToken['token']->setAction(DTA_PRINT_OUT);
                            $tmpToken['token']->setProperties();
                            $tmpToken['token']->setProperty('error', $strError);
                        }
                    }
                    else{
                        $strError = $this->getMessageType($tToken->getType());
                        $tToken->setType(DTT_ERROR);
                        $tToken->setAction(DTA_PRINT_OUT);
                        $tToken->setExtraDataArray();
                        $tToken->setExtraData('error', $strError);
                        $canContinue = FALSE;
                    }
                }while($canContinue);
            }
            elseif($tToken->getType()==DTT_STMT_END_IF){
                $canContinue = TRUE;
                do{
                    $tmpToken = $loopStack->popItem();
                    if ($tmpToken!==NULL){
                        if (($tmpToken['token']->getType()==DTT_STMT_IF)||($tmpToken['token']->getType()==DTT_STMT_ELSE_IF)||($tmpToken['token']->getType()==DTT_STMT_ELSE)){
                            $tmpToken['token']->setProperty('endif', $intIndex);
                            if ($tmpToken['token']->getType()==DTT_STMT_IF)
                                $canContinue = FALSE;
                        }
                        else{
                            $strError = $this->getMessageType($tmpToken['token']->getType());
                            $tmpToken['token']->setType(DTT_ERROR);
                            $tmpToken['token']->setAction(DTA_PRINT_OUT);
                            $tmpToken['token']->setProperties();
                            $tmpToken['token']->setProperty('error', $strError);
                        }
                    }
                    else{
                        $tToken->setType(DTT_ERROR);
                        $tToken->setAction(DTA_PRINT_OUT);
                        $tToken->setProperties();
                        $tToken->setProperty('error', 'IF Statement : stripped END_IF </if>');
                        $canContinue = FALSE;
                    }
                }while($canContinue);
            }
        }
        while(!$loopStack->isEmpty()){
            $tmpToken = $loopStack->popItem();
            $strError = $this->getMessageType($tmpToken['token']->getType());
            $tmpToken['token']->setType(DTT_ERROR);
            $tmpToken['token']->setAction(DTA_PRINT_OUT);
            $tmpToken['token']->setProperties();
            $tmpToken['token']->setProperty('error', $strError);
        }
    }
    private function tagsNormalizer(){
        $arrTokens = $this->arrTokens;
        $this->arrTokens = array();
        $prevToken = null;
        foreach($arrTokens as $token){
            if ($prevToken===NULL)
                $prevToken = $token;
            else{
                if (($prevToken->getType()===$token->getType())&&($token->getType()===DTT_HTML_TAG)){
                    $strTag = $prevToken->getProperty('tag');
                    $strTag .= $token->getProperty('tag');
                    $prevToken->setProperty('tag', $strTag);
                }
                else{
                    $this->arrTokens[] = $prevToken;
                    $prevToken = $token;
                }
            }
        }
        $this->arrTokens[] = $prevToken;
    }
    private function getMessageType($intType){
        switch($intType){
            case DTT_STMT_IF:
                $strError = 'IF Statement : stripped IF <if ...>';
                break;
            case DTT_STMT_ELSE_IF:
                $strError = 'IF Statement : stripped ELSE_IF <else ...>';
                break;
            case DTT_STMT_ELSE:
                $strError = 'IF Statement : stripped ELSE <else>';
                break;
            case DTT_STMT_END_IF:
                $strError = 'IF Statement : stripped END_IF </if>';
                break;
            case DTT_STMT_LOOP:
                $strError = 'LOOP Statement : stripped LOOP <loop ...>';
                break;
            case DTT_STMT_FOR:
                $strError = 'FOR Statement : stripped FOR <for ...>';
                break;
            case DTT_STMT_END_LOOP:
                $strError = 'LOOP Statement : stripped END_LOOP </loop>';
                break;
            case DTT_STMT_END_FOR:
                $strError = 'FOR Statement : stripped END_FOR </for>';
                break;
            default:
                $strError = 'Unknown statement';
        }
        return $strError;
    }
    public function showTokens(){
        foreach($this->arrTokens as $token)
            echo $token, "\n";
    }
    public function resetIndex($intIndexItem=-1){
        if ($intIndexItem<0)
            $this->intIndex = -1;
        else
            $this->intIndex = $intIndexItem;
    }
    public function getTokenCount(){
        return count($this->arrTokens);
    }
    public function currentToken(){
        if (($this->intIndex < 0) || ($this->countToken() === 0))
            return NULL;
        if (isset($this->arrTokens[$this->intIndex]))
            return $this->arrTokens[$this->intIndex];
        return NULL;
    }
    public function nextToken(){
        if (isset($this->arrTokens[$this->intIndex+1]))
            return $this->arrTokens[++$this->intIndex];
        return NULL;
    }
    public function prevToken(){
        if (isset($this->arrTokens[$this->intIndex-1]))
            return $this->arrTokens[--$this->intIndex];
        return NULL;
    }
    public function firstToken(){
        $this->intIndex = -1;
        return $this->nextToken();
    }
    public function lastToken(){
        $this->intIndex = $this->countToken();
        return $this->currentToken();
    }
    public function isEmpty(){
        if (count($this->arrTokens)<=0)
            return TRUE;
        return FALSE;
    }
    public function currentIndex(){
        return $this->intIndex;
    }
    public function itemOver(){
        $arrCount = count($this->arrTokens);
        if ($arrCount<=$this->intIndex+1)
            return TRUE;
        return FALSE;
    }
    public function goToToken($intTokenPosition){
        if ($intTokenPosition<=-1) $this->intIndex=-1;
        elseif ($intTokenPosition>$this->getTokenCount())  $this->intIndex = $intTokenPosition - 1;
        else $this->intIndex = $intTokenPosition - 1;
    }
    public function changeAction($intTokenPosition, $dtaAction){
        if ($intTokenPosition<=-1) return NULL;
        elseif ($intTokenPosition>$this->getTokenCount()-1)  return NULL;
        $this->arrTokens[$intTokenPosition]->setAction($dtaAction);
    }
}

/*
 *********** DO WORK ***********
 *
 * check messages
 * can parsing string use \
 *
 */
